%global srcname autopage

Summary:        A Python library to provide automatic paging for console output
Name:           python-%{srcname}
Version:        0.5.1
Release:        9%{?dist}
License:        ASL 2.0
URL:            https://pypi.python.org/pypi/autopage
Source0:        %{pypi_source}
Source1:        setup.py

BuildArch:      noarch

%description
Autopage is a Python library to provide automatic paging for console output.

%package -n python3-%{srcname}
Summary:        A Python library to provide automatic paging for console output
BuildRequires:  python3-devel python3-setuptools

# for tests
BuildRequires:  %{py3_dist fixtures} %{py3_dist testtools}
BuildRequires:  python3-pytest

%description -n python3-%{srcname}
Autopage is a Python library to provide automatic paging for console output.

%prep
%autosetup -n %{srcname}-%{version}

rm autopage/tests/test_end_to_end.py

%generate_buildrequires
%pyproject_buildrequires

%build
%pyproject_wheel

%install
%pyproject_install
%pyproject_save_files autopage

%check
%pytest \
 -k "not test_defaults and \
     not test_defaults_cmd_as_class and \
     not test_defaults_cmd_as_string"

%files -n python3-%{srcname} -f %{pyproject_files}
%license LICENSE
%doc README.md

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.5.1-9
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Thu Sep 19 2024 Shuo Wang <abushwang@tencent.com> - 0.5.1-8
- switch test from tox to pytest

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.5.1-7
- Rebuilt for loongarch release

* Mon Jun 3 2024 Shuo Wang <abushwang@tencent.com> - 0.5.1-6
- enable %check

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.5.1-5
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.5.1-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.5.1-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.5.1-2
- Rebuilt for OpenCloudOS Stream 23

* Wed Mar 15 2023 Shuo Wang <abushwang@tencent.com> - 0.5.1-1
- initial build
